/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * Stack Overflow
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 5
 */

#include <iostream>
using std::cin;
using std::cout;
using std::getline;
#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <set>
using std::set;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	if(charonly == 0 && linesonly == 0 && wordsonly == 0 && uwordsonly == 0 && longonly == 0)
	{
		linesonly = 1;
		wordsonly = 1;
		charonly = 1;
	}

	string n = "";
	char cRead;
	int lineAmount = 0;
	int wordAmount = 0;
	int byteAmount = 0;
	int lineMaxLength = 0;
	int uniqueWords = 0;

	const int WHITESPACE = 0;
	const int NONWHITESPACE = 1;
	int state = WHITESPACE;
	int prevState = state;

	while(fread(&cRead,1,1,stdin))
	{
		n += cRead;
		if(cRead == ' ' || cRead == '\n' || cRead == '\t' || cRead == '\v' || cRead == '\f' || cRead == '\r')
			state = WHITESPACE;
		else
			state = NONWHITESPACE;

		if(state == WHITESPACE)
		{
			if(prevState == NONWHITESPACE)
			{
				wordAmount++;
				//cout << wordAmount << ": " << n[n.length() - 2] << "\n";
			}
		}

		prevState = state;
	}
	if(n[n.length()-1] != ' ' && n[n.length()-1] != '\n' && n[n.length()-1] != '\t' && n[n.length()-1] != '\v' && n[n.length()-1] != '\f' && n[n.length()-1] != '\r')
		wordAmount++;

	if(linesonly == 1)
	{
		for(int i = 0; i < n.length(); i++)
		{
			if(n[i] == '\n' || (i == n.length() - 1 && lineAmount == 0))
				lineAmount++;
		}
	}

	/*if(wordsonly == 1)
	{
		for(int i = 0; i < n.length(); i++)
		{
			if(n[i] != ' ' && n[i] != '\n' && (i+1 == n.length() - 1 || n[i+1] == ' ' || n[i+1] == '\n'))
			{
				wordAmount++;
			}
		}
	}*/

	if(charonly == 1)
	{
		byteAmount = n.size();
	}

	if(longonly == 1)
	{
		bool longestLast = false;
		int tempMaxLength = 0;
		for(int i = 0; i < n.length(); i++)
		{
			if(n[i] == '\t')
			{
				tempMaxLength += 8 - tempMaxLength % 8;
			}
			else
			{
				tempMaxLength++;
			}
			if(n[i] == '\n' || i == n.length() - 1)
			{
				//cout << tempMaxLength << "\n";
				if(tempMaxLength > lineMaxLength)
				{
					lineMaxLength = tempMaxLength;
					if(i == n.length() - 1)
						longestLast = true;
				}
				tempMaxLength = 0;
			}
		}

		if(longestLast == false)
			lineMaxLength--;
	}

	if(uwordsonly == 1)
	{
		//Create string set. Determine if it's a word by looping until you find a space. If word is not in set, add to set and increase unique word count. Else, do nothing and move on.
		set<string> wordHistory;
		string temp = "";
		for(int i = 0; i < n.length(); i++)
		{
			//cout << "i: " << i << "\n";
			if(isalpha(n[i]) != 0)
			{
				for(int j = i; j < n.length(); j++)
				{
					if(n[j] == ' ' || j == n.length() - 1)
					{
						for(int k = i; k <= j; k++)
						{
							temp += n[k];
						}
						wordHistory.insert(temp);
						temp = "";
						i = j+1;
					}

					//cout << "j: " << j << "\n";
				}
			}
		}

		uniqueWords = wordHistory.size();
	}

	if(linesonly == 1)
		cout << lineAmount << "\t";

	if(wordsonly == 1)
		cout << wordAmount << "\t";

	if(charonly == 1)
		cout << byteAmount << "\t";

	if(longonly == 1)
		cout << lineMaxLength << "\t";

	if(uwordsonly == 1)
		cout << uniqueWords << "\t";

	cout << "\n";

	return 0;
}