#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;



static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[]) {
    // define long options
    static int echo=0, rlow=0, rhigh=0;
    static size_t count=-1;
    bool userange = false;
    static struct option long_opts[] = {
        {"echo",        no_argument,       0, 'e'},
        {"input-range", required_argument, 0, 'i'},
        {"head-count",  required_argument, 0, 'n'},
        {"help",        no_argument,       0, 'h'},
        {0,0,0,0}
    };
    // process options:
    char c;
    int opt_index = 0;
    while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
        switch (c) {
            case 'e':
                echo = 1;
                break;
            case 'i':
                if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
                    fprintf(stderr, "Format for --input-range is N-M\n");
                    rlow=0; rhigh=-1;
                } else {
                    userange = true;
                }
                break;
            case 'n':
                count = atol(optarg);
                break;
            case 'h':
                printf(usage,argv[0]);
                return 0;
            case '?':
                printf(usage,argv[0]);
                return 1;
        }
    }
    /* NOTE: the system's shuf does not read stdin *and* use -i or -e.
     * Even -i and -e are mutally exclusive... */

    /* TODO: write me... */


vector<int> random;
vector<string> input;
vector<string> str;


if (echo==1){

while (optind < argc){
input.push_back(argv[optind++]);

}

if (input.size()>1){ //shuffle
srand(time(0));

for (size_t i=0; i<input.size();i++){

    swap(input[i],input[(rand()%(input.size()))]);

}
}


if((int)count<0){

    for(size_t i=0;i<input.size();i++){
        cout<<input[i]<<"\n";

    }
}
else{

    for(size_t i=0; i<count;i++){
    cout<<input[i]<<"\n";
    }

}

    }
else if(userange==true){

        if(rlow>rhigh){
        swap(rlow,rhigh);

    }


    for(int i=rlow; i<rhigh+1;i++){
        random.push_back(i);


    }



if(random.size()!=1){

    srand(time(0));
    for (size_t i=0; i<random.size();i++){


    swap(random[i],random[(rand()%(random.size()))]);

}
}



if((int)count<0){

    for(size_t i=0;i<random.size();i++){
        cout<<random[i]<<"\n";
    }
}

else{

    for(int i=0; i<(int)count;i++){
    cout<<random[i]<<"\n";
    }

}

    }
else{

string usi;

     while(getline(cin,usi)){
str.push_back(usi);
    }

if(str.size()!=1){
    srand(time(0));
    for (size_t i=0; i<str.size();i++){

    swap(str[i],str[(rand()%(str.size()))]);

}
}

if((int)count<0){

    for(size_t i=0;i<str.size();i++){
        cout<<str[i]<<"\n";
    }
}
else{
    for(size_t i=0; i<count;i++){
    cout<<str[i]<<"\n";
    }

}


}
return 0;



}



